//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace KalenderTabelina
{
    using System;
    using System.Collections.Generic;
    
    public partial class Kalender
    {
        public int Id { get; set; }
        public int KasutajaId { get; set; }
        public System.DateTime Kuupäev { get; set; }
        public string Pealkiri { get; set; }
        public string Sisu { get; set; }
    }
}
