﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using KalenderTabelina;

namespace KalenderTabelina.Controllers
{
    public class KalenderController : Controller
    {
        private UusRaamatEntities db = new UusRaamatEntities();

        // GET: Kalenders
        public ActionResult Index()
        {
            return View(db.Kalender.ToList());
        }

        public ActionResult Kuu(int? id)
        {
            DateTime thisDay = DateTime.Now.Date.AddDays(id ?? 0);
            DateTime startDay = thisDay.AddDays(1 - thisDay.Day);
            DateTime startRow = startDay.AddDays(0 - (int)(startDay.DayOfWeek));
            ViewBag.ThisDay = thisDay;
            ViewBag.StartDay = startDay;
            ViewBag.StartRow = startRow;
            ViewBag.KalenderId = id ?? 0;

            return View(db.Kalender.ToLookup(x => x.Kuupäev).ToDictionary(x => x.Key, x => x.ToList()));
        }

        // GET: Kalenders/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Kalender kalender = db.Kalender.Find(id);
            if (kalender == null)
            {
                return HttpNotFound();
            }
            return View(kalender);
        }

        // GET: Kalenders/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Kalenders/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,KasutajaId,Kuupäev,Pealkiri,Sisu")] Kalender kalender)
        {
            if (ModelState.IsValid)
            {
                db.Kalender.Add(kalender);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(kalender);
        }

        // GET: Kalenders/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Kalender kalender = db.Kalender.Find(id);
            if (kalender == null)
            {
                return HttpNotFound();
            }
            return View(kalender);
        }

        // POST: Kalenders/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,KasutajaId,Kuupäev,Pealkiri,Sisu")] Kalender kalender)
        {
            if (ModelState.IsValid)
            {
                db.Entry(kalender).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(kalender);
        }

        // GET: Kalenders/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Kalender kalender = db.Kalender.Find(id);
            if (kalender == null)
            {
                return HttpNotFound();
            }
            return View(kalender);
        }

        // POST: Kalenders/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Kalender kalender = db.Kalender.Find(id);
            db.Kalender.Remove(kalender);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
